import collections
import re
import pickle


class User(collections.namedtuple('User', 'email username real_name id')):
    def display_name(self):
        if self.username is not None:
            real_name = (self.real_name + ' ') if self.real_name else ''
            return real_name + '`@{}`'.format(self.username)
        elif self.real_name:
            return self.real_name
        return '{}..@..{}'.format(self.email[:3], self.email[-6:])


class UserCache:
    def __init__(self, target, bugzilla, product):
        self._target = target
        self._bugzilla = bugzilla
        self._gitlab_emails_cache = {}
        self._users_cache = {}

        self._gitlab_emails_cache = self._retrieve_gitlab_emails_cache()
        self._save_gitlab_emails_cache()

        components = self._bugzilla.getcomponentsdetails(product)
        self._default_emails = set(c['default_assigned_to']
                                   for c in components.values())

    def __getitem__(self, email):
        # Default assignees on GNOME projects don't correspond to a GitLab
        # user, and effectively mean unassigned
        # Except fd.o doesn't use these.
        # if email in self._default_emails:
            # return None

        if email in self._users_cache:
            return self._users_cache[email]

        if email in self._gitlab_emails_cache:
            gitlab_user_id = self._gitlab_emails_cache[email]
            gitlab_user = self._target.find_user(gitlab_user_id)
            user = User(email=email, username=gitlab_user.username,
                        real_name=gitlab_user.name, id=gitlab_user.id)
            self._users_cache[email] = user
            return user

        bzu = self._bugzilla.getuser(email)
        # Heuristically remove "(not reading bugmail) or (not receiving
        # bugmail)"
        real_name = re.sub(r' \(not .+ing bugmail\)', '', bzu.real_name)
        user = User(email=email, real_name=real_name, username=None, id=None)
        self._users_cache[email] = user

        return user

    def _retrieve_gitlab_emails_cache(self):
        gitlab_emails_cache = {}
        try:
            with open('users_cache', 'rb') as fp:
                print('Loading users from \'users_cache\' file')
                gitlab_emails_cache = pickle.load(fp)
        except Exception as error:
            print('Initializing users_cache with users from nsnam')
            gitlab_emails_cache['krotov@iitp.ru'] = 1262351#'link2xt'
            gitlab_emails_cache['adadeepak8@gmail.com'] = 2471823 #'adeepkit01'
            gitlab_emails_cache['biljana.bojovic@gmail.com'] = 3254437 #'biljkus'
            gitlab_emails_cache['gjcarneiro@gmail.com'] = 1313534 # 'gjcarneiro'
            gitlab_emails_cache['manuel.requena@cttc.es'] = 3034205 # 'mrequena'
            gitlab_emails_cache['natale.patriciello@gmail.com'] = 2478411 #'natale-p'
            gitlab_emails_cache['barnes26@llnl.gov'] = 1138790 # 'pdbj'
            gitlab_emails_cache['sebastien.deronne@gmail.com'] = 3148397 #'sderonne'
            gitlab_emails_cache['stavallo@unina.it'] =  2791497#'stavallo'
            gitlab_emails_cache['tomh@tomh.org'] = 2480280#'tomhenderson'
            gitlab_emails_cache['tommaso.pecorella@unifi.it'] = 3252122# 'tommypec'
            gitlab_emails_cache['rivanvx@gmail.com'] = 3269492#'vedranmiletic'
            gitlab_emails_cache['zoraze.ali@cttc.es'] = 3234414 #'ZorazeAli'

        return gitlab_emails_cache

    def _save_gitlab_emails_cache(self):
        with open('users_cache', 'wb') as fp:
            pickle.dump(self._gitlab_emails_cache, fp)
            print('Wrote users data into file \'users_cache\' for caching \
purposes')
